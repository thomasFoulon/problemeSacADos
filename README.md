# RESOLUTION DU PROBLEME DU SAC A DOS

## Probl�me du sac � dos :

https://fr.wikipedia.org/wiki/Probl%C3%A8me_du_sac_%C3%A0_dos

## Utilisation:

java -jar problemeSacADos.jar cheminDesObjets poidsMaximal m�thode

## 3 m�thodes de r�solution du probl�me du sac � dos :
* 1 - M�thode gloutonne (ligne de commande : glouton)
* 2 - M�thode dynamique (ligne de commande : dynamique)
* 3 - M�thode PSE (ligne de commande : PSE)